package configuration

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog"
	"go.dev.pztrn.name/stabonator/internal/logger"
	"gopkg.in/yaml.v2"
)

var (
	Cfg *config
	log zerolog.Logger
)

// Initialize initializes package.
func Initialize() {
	log = logger.Logger.With().Str("type", "internal").Str("domain", "configuration").Logger()
	log.Info().Msg("Initializing...")

	Cfg = &config{}

	pathRaw, found := os.LookupEnv("STABONATOR_CONFIG")
	if !found {
		log.Fatal().Msg("Failed to read configuration - no STABONATOR_CONFIG environment variable defined.")
	}

	// Normalize path.
	if strings.HasPrefix(pathRaw, "~") {
		userHomeDir, err := os.UserHomeDir()
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to obtain user's home directory path")
		}

		pathRaw = strings.Replace(pathRaw, "~", userHomeDir, 1)
	}

	absPath, err1 := filepath.Abs(pathRaw)
	if err1 != nil {
		log.Fatal().Err(err1).Msg("Failed to get absolute path for configuration file")
	}

	// Read and parse configuration file.
	fileData, err2 := ioutil.ReadFile(absPath)
	if err2 != nil {
		log.Fatal().Err(err2).Msg("Failed to read configuration file data")
	}

	Cfg = &config{}

	err3 := yaml.Unmarshal(fileData, Cfg)
	if err3 != nil {
		log.Fatal().Err(err3).Msg("Failed to parse configuration file")
	}

	log.Info().Msgf("Configuration file parsed: %+v", Cfg)
}
