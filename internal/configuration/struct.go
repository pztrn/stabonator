package configuration

// This is a structure that represents configuration file structure.
type config struct {
	// HTTPServers represents httpserver domain configuration.
	HTTPServers map[string]APIServer `yaml:"httpserver"`
}

// APIServer represents single API server configuration.
// This configuration structure is shared between all API servers
// implementations.
type APIServer struct {
	// ListenAddress is an address this API server will listen to.
	// Should be unique.
	ListenAddress string `yaml:"listen_address"`
	// Methods represents available methods structure.
	Methods []APIServerMethods `yaml:"methods"`
}

// APIServerMethods represents available API methods for every API
// server. This structure is shared between all API servers
// implementations.
type APIServerMethods struct {
	// Method is a method name. Think about this like "GET"/"POST"/etc
	// for HTTP.
	Method string `yaml:"method"`
	// Path is a path on which this method will reply (and receive
	// data if applicable).
	Path string `yaml:"path"`
	// Response is a response we will send back to requester.
	Response string `yaml:"response"`
	// ResponseType is a type of response if needed. Think about this
	// like "application/json" for HTTP.
	ResponseType string `yaml:"response_type"`
	// ConditionalResponses describes conditional request-reply pairs.
	// If ConditionalResponses is defined in configuration file for API
	// server - it'll take precedence over previous parameters.
	ConditionalResponses []ConditionalResponse `yaml:"conditional_responses"`
}

// ConditionalResponse represents conditional responses where condition
// in a request. This is useful when you want to test interoperability
// with poorly designed APIs which might return data using different
// schemas.
type ConditionalResponse struct {
	// ReceivedData is a request we receiving.
	ReceivedData string `yaml:"received_data"`
	// ResponseType is a type of response if needed. Think about this
	// like "application/json" for HTTP. This overrides response type
	// defined in APIServerMethods.ResponseType.
	ResponseType string `yaml:"response_type"`
	// Reply is a reply we will send back.
	Response string `yaml:"response"`
}
