package logger

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/rs/zerolog"
)

var (
	Logger         zerolog.Logger
	SuperVerbosive bool
)

// Initialize initializes zerolog with proper formatting and log level.
func Initialize() {
	// Check environment for logger level.
	// Defaulting to INFO.
	loggerLevel, loggerLevelFound := os.LookupEnv("LOGGER_LEVEL")
	if loggerLevelFound {
		switch strings.ToUpper(loggerLevel) {
		case "DEBUG":
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		case "INFO":
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		case "WARN":
			zerolog.SetGlobalLevel(zerolog.WarnLevel)
		case "ERROR":
			zerolog.SetGlobalLevel(zerolog.ErrorLevel)
		case "FATAL":
			zerolog.SetGlobalLevel(zerolog.FatalLevel)
		}
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	output := zerolog.ConsoleWriter{Out: os.Stdout, NoColor: true, TimeFormat: time.RFC3339}
	output.FormatLevel = func(i interface{}) string {
		var v string

		if ii, ok := i.(string); ok {
			ii = strings.ToUpper(ii)
			switch ii {
			case "DEBUG":
				v = fmt.Sprintf("%-5s", ii)
			case "ERROR":
				v = fmt.Sprintf("%-5s", ii)
			case "FATAL":
				v = fmt.Sprintf("%-5s", ii)
			case "INFO":
				v = fmt.Sprintf("%-5s", ii)
			case "PANIC":
				v = fmt.Sprintf("%-5s", ii)
			case "WARN":
				v = fmt.Sprintf("%-5s", ii)
			default:
				v = ii
			}
		}

		return fmt.Sprintf("| %s |", v)
	}

	Logger = zerolog.New(output).With().Timestamp().Logger()
}
