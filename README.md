# Stabonator

Stabonator is a simple service that able to mock external APIs using data provided in configuration file. It is able to set many API servers.

Currently supported protocols: only HTTP.

## Installation

```bash
go get -u -v go.dev.pztrn.name/stabonator
```

Check ``$GOPATH/bin`` for ``stabonator`` binary.

## Configuring

While [documentation](doc/index.md) is still WIP you can take a look at [example config](examples/stabonator.yaml) for configuration file structure and possible variables.

Path to configuration file should be specified in ``STABONATOR_CONFIG`` environment variable.

## Developing and bug reporting

Stabonator is developed on [my Gitea instance](https://sources.dev.pztrn.name/pztrn/stabonator). If you encounter this repository on gitlab.com - you reached repository mirror. Please go to [my Gitea](https://sources.dev.pztrn.name/pztrn/stabonator) for bug reporting.