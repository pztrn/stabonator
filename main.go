package main

import (
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"go.dev.pztrn.name/stabonator/internal/configuration"
	"go.dev.pztrn.name/stabonator/internal/logger"
	"go.dev.pztrn.name/stabonator/services/httpserver/echo"
)

func main() {
	// Locking one system thread for proper signals handling.
	runtime.LockOSThread()

	logger.Initialize()
	configuration.Initialize()

	echo.Initialize()

	// CTRL+C handler.
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT)

	shutdownDone := make(chan bool, 1)

	go func() {
		signalThing := <-interrupt
		if signalThing == syscall.SIGTERM || signalThing == syscall.SIGINT {
			logger.Logger.Info().Msg("Got " + signalThing.String() + " signal, shutting down...")
			echo.Shutdown()
			shutdownDone <- true
		}
	}()

	<-shutdownDone
	os.Exit(0)
}
