package echo

import (
	"context"
	"net/http"
	"strconv"
	"time"

	echo4 "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/zerolog"
	"go.dev.pztrn.name/stabonator/internal/configuration"
	"go.dev.pztrn.name/stabonator/internal/logger"
)

var (
	httpServers []*echo4.Echo
	log         zerolog.Logger
)

// Initialize initializes domain.
func Initialize() {
	log = logger.Logger.With().Str("domain", "httpserver").Int("version", 1).Logger()
	log.Info().Msg("Initializing...")

	httpServers = make([]*echo4.Echo, 0, len(configuration.Cfg.HTTPServers))

	// Initializing HTTP server and endpoint handlers.
	for name, cfg := range configuration.Cfg.HTTPServers {
		log.Info().Str("name", name).Str("address", cfg.ListenAddress).Msg("Creating new HTTP API server...")

		e := echo4.New()
		e.Use(middleware.Recover())
		e.Use(createLogger())
		e.DisableHTTP2 = true
		e.HideBanner = true
		e.Debug = true
		e.Binder = &StrictJSONBinder{}

		e.HTTPErrorHandler = defaultErrorHandler
		e.Any("/*", defaultRouteHandler)

		for _, worker := range cfg.Methods {
			log.Debug().Str("server", name).Str("method", worker.Method).Str("path", worker.Path).Msg("Adding handler")
			handler := createHandler(worker)

			switch worker.Method {
			case http.MethodDelete:
				e.DELETE(worker.Path, handler)
			case http.MethodGet:
				e.GET(worker.Path, handler)
			case http.MethodPatch:
				e.PATCH(worker.Path, handler)
			case http.MethodPost:
				e.POST(worker.Path, handler)
			case http.MethodPut:
				e.PUT(worker.Path, handler)
			}
		}

		// nolint
		go e.Start(cfg.ListenAddress)

		httpServers = append(httpServers, e)
	}
}

// Requests logger.
func createLogger() echo4.MiddlewareFunc {
	return func(next echo4.HandlerFunc) echo4.HandlerFunc {
		return func(ec echo4.Context) error {
			return reqLogger(ec, next)
		}
	}
}

func reqLogger(reqctx echo4.Context, next echo4.HandlerFunc) error {
	startTime := time.Now()

	_ = next(reqctx)

	reqTime := time.Since(startTime)

	// Avoiding exponents in log output.
	reql := strconv.FormatInt(reqctx.Request().ContentLength, 10)
	resl := strconv.FormatInt(reqctx.Response().Size, 10)

	log.Info().
		Str("module", "echo").
		Str("IP", reqctx.RealIP()).
		Str("Method", reqctx.Request().Method).
		Str("Request length", reql).
		Str("Response length", resl).
		Str("Remote", reqctx.Request().RemoteAddr).
		Str("URI", reqctx.Request().RequestURI).
		Dur("Request time (s)", reqTime).
		Msgf("HTTP request")

	return nil
}

// HTTP servers shutdown.
func Shutdown() bool {
	log.Info().Msg("Shutting down HTTP servers...")

	for _, srv := range httpServers {
		// I know this is not enough to do only Shutdown() call,
		// this is in ToDo.
		err := srv.Shutdown(context.Background())
		if err != nil {
			log.Error().Err(err).Msg("Failed to shutdown HTTP server")
		}
	}

	return true
}
