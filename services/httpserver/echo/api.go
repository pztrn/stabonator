package echo

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	echo4 "github.com/labstack/echo/v4"
	"go.dev.pztrn.name/stabonator/internal/configuration"
)

const (
	contentTypeJSON = "application/json"
	contentTypeText = "text/plain"
)

// This function returns generic API handler that we can use for handling
// HTTP requests.
// nolint:gocognit
func createHandler(cfg configuration.APIServerMethods) echo4.HandlerFunc {
	return func(ec echo4.Context) error {
		// Figure out what we want as content-type.
		acceptableContentTypes := []string{strings.ToLower(cfg.ResponseType)}

		for _, conditional := range cfg.ConditionalResponses {
			var alreadyAdded bool

			for _, act := range acceptableContentTypes {
				if act == conditional.ResponseType {
					alreadyAdded = true

					break
				}
			}

			if !alreadyAdded {
				acceptableContentTypes = append(acceptableContentTypes, strings.ToLower(conditional.ResponseType))
			}
		}

		// Check if request we've received came with acceptable
		// content-type.
		var requestWithAcceptableCT bool

		for _, act := range acceptableContentTypes {
			if strings.Contains(strings.ToLower(ec.Request().Header.Get("content-type")), act) {
				requestWithAcceptableCT = true

				break
			}
		}

		// Send back error if request with unacceptable content-type.
		if !requestWithAcceptableCT {
			defaultErrorHandler(nil, ec)

			return nil
		}

		// Do magic with request :).
		var (
			body         []byte
			err          error
			response     string
			responseType string
		)

		// We should read body only if request's method isn't GET.
		// By standard GET requests should not contain body at all.
		// nolint:nestif
		if cfg.Method != http.MethodGet {
			body, err = ioutil.ReadAll(ec.Request().Body)
			if err != nil {
				return echo4.NewHTTPError(http.StatusInternalServerError, "Failed to read request's body")
			}

			ec.Request().Body.Close()
			log.Debug().Msgf("Request body parsed: %s", body)

			// Check if request is allowed based on body's content.
			if len(cfg.ConditionalResponses) == 0 {
				response = cfg.Response
				responseType = cfg.ResponseType
			} else {
				for _, allowedRequest := range cfg.ConditionalResponses {
					if string(body) == allowedRequest.ReceivedData {
						response = allowedRequest.Response
						responseType = allowedRequest.ResponseType
						log.Debug().Str("response type", responseType).Msgf("Found appropriate reply: %s", response)

						break
					}
				}
			}
		} else {
			// If we have received GET method - we will look only on
			// passed content-type to know what response to use.
			if len(cfg.ConditionalResponses) == 0 {
				response = cfg.Response
				responseType = cfg.ResponseType
			} else {
				// As GET method should not contain body we will check
				// only for passed content-type header.
				for _, allowedRequest := range cfg.ConditionalResponses {
					if allowedRequest.ResponseType == strings.ToLower(ec.Request().Header.Get("content-type")) {
						response = allowedRequest.Response
						responseType = allowedRequest.ResponseType
					}
				}
			}
		}

		// Do some magic with response.
		// If we should return JSON - then return JSON :)
		jsonResponse := map[string]interface{}{}

		if responseType == contentTypeJSON && len(response) != 0 {
			err := json.Unmarshal([]byte(response), &jsonResponse)
			if err != nil {
				return ec.JSON(
					http.StatusInternalServerError,
					map[string]string{
						"error": "Failed to prepare JSON response: " + err.Error(),
					},
				)
			}
		}

		// nolint:nestif
		if len(response) == 0 {
			if responseType == contentTypeText {
				err = ec.String(http.StatusBadRequest, "This request's body isn't allowed")
			} else if responseType == contentTypeJSON {
				err = ec.JSON(http.StatusBadRequest, map[string]string{"error": "This request's body isn't allowed"})
			}
		} else {
			if responseType == contentTypeText {
				err = ec.String(http.StatusOK, response)
			} else if responseType == contentTypeJSON {
				err = ec.JSON(http.StatusOK, jsonResponse)
			}
		}

		return err
	}
}

// This function is a default error handler and executes if any error
// was raised panic or if no endpoint handler exists.
func defaultErrorHandler(err error, ec echo4.Context) {
	_ = ec.JSON(
		http.StatusBadRequest,
		map[string]string{
			"error": "Unknown API path or method for this path. Please define required paths and methods in configuration file.",
		},
	)
}

// This function called if no API handler exists.
func defaultRouteHandler(ec echo4.Context) error {
	defaultErrorHandler(nil, ec)

	return nil
}
