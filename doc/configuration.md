# Stabonator configuration

This file describes Stabonator configuration file structure.

## Configuration file format

Stabonator uses YAML as configuration file format. It is easily readable and exchangeable format, see [YAML specs](https://yaml.org/) if you're unfamiliar with it.

## The Example

Here goes example of HTTP API mocking that mocks two endpoints using two different HTTP methods. One of endpoints is able to answer depending on input data. This is a snip from [example configuration file](/examples/stabonator.yaml).

```yaml
# This will configure HTTP servers.
httpserver:
  # Every server here named, server name appears in logs.
  apiserver1:
    # This is an address on which this HTTP server will listen on.
    listen_address: "127.0.0.1:6000"
    # HTTP methods description. All other methods will return error
    # in plain text or JSON (depending on request's content-type
    # header).
    methods:
      # This is a list of methods. We call them workers.
        # HTTP method name. All caps please.
      - method: "GET"
        # HTTP path on which this worker will listen.
        path: "/api/v1/hello"
        # What response's content-type we'll use.
        response_type: "text/plain"
        # Response itself.
        response: "Hello World"
        # Same as previous, HTTP method, all caps.
      - method: "POST"
        # Same as previous, HTTP path.
        path: "/api/v1/bye"
        # Same as previous, response's context-type.
        response_type: "application/json"
        # Conditional responses is a responses that will be returned
        # based on conditions. Currently only body comparation works.
        # Every conditional response has 3 variables - received body,
        # on which it'll trigger, renspose's context-type and response's
        # body.
        # These parameters obsoletes parent-level parameters
        # response_type and response.
        conditional_responses:
          - received_data: '{"user": "root"}'
            response_type: "application/json"
            response: '{"reply": "gr34t r00t"}'
          - received_data: '{"user": "user"}'
            response_type: "application/json"
            response: '{"reply": "slave!"}'
```

## Possible keys

All possible configuration file keys described in table below.

All keys are formatted as ``key1 > key2 > key3`` with ``>`` as nesting delimiter, which results in this YAML:

```yaml
key1:
  key2:
    key3: "foobar"
```

### httpserver

``httpserver`` is a special top-level key. All nested elements describes HTTP server mocking configuration.

| Key | Expected value | Description |
| --- | -------------- | ----------- |
| httpserver > {name} | String | HTTP server name as appeared in logs. Replace ``{name}`` with your name. |
| httpserver > {name} > listen_address | String in form of ``addr:port`` | Address and port on which HTTP server will listen for requests. |

#### Methods

Every HTTP server should describe own set of available methods. Below is a list of keys that you can define in ``httpserver > {name} > methods`` list. For convenience ``{elem}`` is added to represent list element.

| Key | Expected value | Description |
| --- | -------------- | ----------- |
| httpserver > {name} > methods > {elem} > method | HTTP method name | HTTP method this configuration part will work with. |
| httpserver > {name} > methods > {elem} > path | String | Path on which this configuration should be applied. |
| httpserver > {name} > methods > {elem} > response_type | String | Content-Type header value for response. |

Next fields are mutually exclusive, means than one forbids another.

If there is a need in only one response scheme you should use following:

| Key | Expected value | Description |
| --- | -------------- | ----------- |
| httpserver > {name} > methods > {elem} > response | String | Response that will be sent on request. |

If there is a need in more than one response scheme you should define a list of conditional responses. For convenience ``{resp_elem}`` is added to represent conditional response list element.

| Key | Expected value | Description |
| --- | -------------- | ----------- |
| httpserver > {name} > methods > {elem} > conditional_responses > {resp_elem} > received_data | String | Data that should be received to apply this configuration. |
| httpserver > {name} > methods > {elem} > conditional_responses > {resp_elem} > response_type | String | Content-Type header value. Effectively replaces value defined on level above. |
| httpserver > {name} > methods > {elem} > conditional_responses > {resp_elem} > response | String | Data that should be sent on request. |