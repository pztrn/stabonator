module go.dev.pztrn.name/stabonator

go 1.13

require (
	github.com/labstack/echo/v4 v4.1.17
	github.com/rs/zerolog v1.20.0
	gopkg.in/yaml.v2 v2.3.0
)
